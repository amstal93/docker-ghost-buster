#!/usr/bin/env bash
set -e
source .env

# adding the Disqus universal embed code in the post.hbs template of Casper theme using a hanldebars partial
sed -e "s|DISQUS_SRC|${DISQUS_SRC}|" disqus-universal-embed-code.hbs > themes/casper/partials/disqus-embed.hbs
EMBED_CODE='{{> "disqus-embed"}}'
sed -e "s|^            {{!--$||" -e "s|^            --}}||" -e "s|If you want to embed comments, this is a good place to do it!|${EMBED_CODE}|" themes/casper/post.hbs > post.hbs.disqus
mv post.hbs.disqus themes/casper/post.hbs

docker-compose up -d ghost
docker-compose up -d backup
sleep 10
open http://localhost:2368/ghost/
