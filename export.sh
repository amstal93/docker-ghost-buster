#!/usr/bin/env bash
set -e
source .env

mkdir -p static/public/tag
pushd .
cd static/public/tag
grep -o -E  -e "https://pommetab.com/tag/\w+/"  ../sitemap-tags.xml | cut -d'/' -f 5 | xargs mkdir -p
popd
docker-compose run buster
docker-compose run fixlinks
docker-compose run fixrss