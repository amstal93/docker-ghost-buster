# docker-ghost-buster

A project that runs [Ghost](https://ghost.org/), [Buster](https://github.com/rija/buster), [Ghost-backup](https://github.com/bennetimo/ghost-backup/) and [Nginx](https://nginx.org/) as [Docker](https://www.docker.com/) containers stack to generate and publish the Ghost's blog content as static web pages to [GitLab Pages](https://about.gitlab.com/features/pages/).

Inspired by the following similar projects:

* [StefanScherer/ghost-buster-docker](https://github.com/StefanScherer/ghost-buster-docker)
* [umputun/ghost-buster-docker](https://github.com/umputun/ghost-buster-docker)
* [racccoonyy/docker-compose-ghost-buster](https://github.com/raccoonyy/docker-compose-ghost-buster)


Has following characteristics:

* customised through environment variables in an ``.env`` file
* ``.env`` file is used by both the bash scripts and docker-compose
* compose appropriate docker containers to implement functionality
* therefore, it aims to use official images, lightweight images with single process/responsibility

The Casper theme is submoduled in this project and bind-mounted to the appropriate location inside the Ghost container.
Therefore you can edit and customise the theme at will in the ``themes/casper`` directory.

The supplied configuration file **config.production.json** is bind-mounted to the Ghost container allowing you to edit Ghost configuration from outside the container.


*Be careful, It's an early, rough, work in progress.*


## Usage

### Configuring environment variables

```
$ cp env-sample .env
```

then edit the following variables to fit your context:

``REMOTE_DOMAIN``: The name of the target web site

``GHOST_PORT``: The port to access the Ghost application. Default value is correct for normal cases.

``GHOST_INSTALL``: The install directory for Ghost. The config file goes there. Default value is correct for normal cases.

``GHOST_CONTENT``: The directory where Ghost's content is located. Default value is correct for normal cases.

``GHOST_LOCATION``: Used by ghost-backup to find Ghost's install location. Default value is correct for normal cases.

``PREVIEW_PORT``: The port to access the Nginx server

``DISQUS_SRC``: The url of a DISQUS account - formatted as https://something.disqus.io -, leave empty if not relevant

``BACKUP_PATH``: Location on your host where ghost-backup stores the backup files for your Ghost install

``GIT_REMOTE``: The url to the GitLab repo to host the pages


### Initialising the ``static`` output directory and cloning the Casper theme

```
$ ./setup.sh
```

This will create the remote repository if it doesn't exist yet.

>**Note:** this script can be run any number of times safely.


### Starting Ghost for editing

```
$ ./start.sh
```

The Ghost application will be available at ``http://localhost:$GHOST_PORT``

This script also starts the backup container server that will automatically create a backup of the Ghost database and
of Ghost files content every night at 3am.

>**Note:** this script can be run any number of times safely. It will recreate the ghost container if it is already running. The content directory, configuration file and  the theme directory are unaffected.

### Exporting content as static pages

```
$ ./export.sh
```


### Exporting content as static pages and preview locally

```
$ ./preview.sh
```

### Exporting content as static pages and publish to GitLab

```
$ ./publish.sh
```

### Manually backup your Ghost install

```
$ ./backup.sh
```

Manually creates database backup and files backup at the location defined by $BACKUP_PATH. Two tarball archives will be created for each backup session: one for the database and one for the Ghost files under ``content``. The files have the date and time appended to the filename. The **config.production.json** configuration file is also part of the Ghost files backup archive.

### Restore your Ghost install from a backup

```
$ ./restore.sh
```

With this interactive script, It will show the list of available backups of each type (database and files) and you choose wich one to restore. The **config.production.json** configuration file is not restored by this script. Use the script below if you need to restore the configuration file.

### Restore your configuration file from a Ghost files backup archive

```
$ ./restore_config.sh backup-ghost_YYYYMMDD-HHMM.tar.gz
```

This script will restore the **config.production.json** configuration file from a Ghost files backup archive.
You can get the list of available backup by running the interactive ``./restore.sh`` script and then ``CTRL-C`` it.


## Setting up comments with Disqus

If order to install Disqus commenting on every post, make sure DISQUS\_SRC in your ``.env`` file is set with your Disqus URL before you start the Ghost container (with ``./start_blog.sh``). This only works if you use the default theme (Casper) though.

## Customising Casper theme

The Casper theme is git submoduled in this project directory, either after running ``setup.sh`` or manually with ``git submodule update``. It is then bind-mounted inside the themes directory in the container.
Any change done to the submodule will be reflected inside the container.

## TODO

* add a file system watcher that automatically publish when something changes in Ghost database
* allow other themes
